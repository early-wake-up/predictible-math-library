#include "Predictable_Math.h"

// -----------------------------------------------------------
// LUT for sine and asin
// Simply include the file, to benefit from value of N directly
// -----------------------------------------------------------
#include "lut.cc"

// -----------------------------------------------------------
// Predictable sinus using LUT in interval [0,PI/2]
// -----------------------------------------------------------
double predictable_sin_zero_pi_2(double angle) {

  // Sanity check, for now angle should be in interval [0,PI/2]
  assert (angle >=0 && angle <= M_PI_2);

  // Actual calculation using linear regression
  double interval_length = M_PI_2 / (double)(N-1);
  double d_index = angle / interval_length;
  int index_start = (int) d_index;
  assert (index_start < N);
  int index_end;
  if ((d_index - (double) index_start > 0.0) && (index_start!=(N-1))) index_end=index_start+1;
  else index_end=index_start;

  // Linear regression in the interval
  if (index_start == index_end) return lut_sin[index_start];
  double a = lut_sin[index_end] - lut_sin[index_start];
  double b = lut_sin[index_start] - a*index_start;
  return a*d_index+b;

}

// -----------------------------------------------------------
// Predictable sinus using LUT in interval [0,PI]
// -----------------------------------------------------------
double predictable_sin_zero_pi(double angle) {

  // Sanity check, angle should be in interval [0,PI]
  assert (angle >=0 && angle <= M_PI);

  // Exploit property sin(pi-x) = sin(x);
  if (angle >=M_PI_2) return predictable_sin_zero_pi_2(M_PI-angle);
  else return predictable_sin_zero_pi_2(angle);

}

// -----------------------------------------------------------
// Predictable sinus using LUT, the angle is random
// -----------------------------------------------------------
double predictable_sin(double angle) {
  
   // Put angle in interval [0..2PI] first
  long nb=0;
  if (angle > 2*M_PI) {
    nb = angle / (2*M_PI);
    angle = angle - nb*2*M_PI;
  }
  if (angle <0) {
    nb = -1*angle / (2*M_PI) +1;
    angle = angle + nb*2*M_PI;
  }
  
  // Sanity check (could be removed)
  assert (angle >=0 && angle <= 2*M_PI);
  
  // Exploit property sin(-x) = -sin(x) to be in interval [0,PI]
  if (angle >= M_PI) return (double)(-1) * predictable_sin_zero_pi(2*M_PI-angle);
  else return predictable_sin_zero_pi(angle);
}


// -----------------------------------------------------------
// Predictable cosinus using LUT, the angle is random
// -----------------------------------------------------------
double predictable_cos(double angle) {
  // Exploit property cos(x) = sin(PI/2-x) 
  return predictable_sin(M_PI_2-angle);
}

// -----------------------------------------------------------
// Predictable arc sine using LUT, parameter in [0,1]
// -----------------------------------------------------------
double predictable_asin_0_1(double x) {
  // Sanity check x should be in interval [0,1]
  assert(x>=0.0 && x<=1.0);
  // Actual calculation using linear regression
  double interval_length = 1.0 / (double)(N-1);
  double d_index = x / interval_length;
  int index_start = (int) d_index;
  assert (index_start < N);
  int index_end;
  if ((d_index - (double) index_start > 0.0) && (index_start!=(N-1))) index_end=index_start+1;
  else index_end=index_start;

  // Linear regression in the interval
  if (index_start == index_end) return lut_asin[index_start];
  double a = lut_asin[index_end] - lut_asin[index_start];
  double b = lut_asin[index_start] - a*index_start;
  return a*d_index+b;
}

// -----------------------------------------------------------
// Predictable arc sine using LUT, parameter is random
// -----------------------------------------------------------
double predictable_asin(double x) {
  // Error check for x
  if (x<-1.0 || x>1.0) return NAN;
  // Exploit the fact that asin(-x)=-asin(x);
  if (x<0.0) return -1.0*predictable_asin_0_1(-1.0*x);
  else return predictable_asin_0_1(x);
}

// -----------------------------------------------------------
// Predictable arc cos using LUT, parameter is random
// -----------------------------------------------------------
double predictable_acos(double x) {
  // Exploit the fact that acos(x)=PI/2 - asin(x);
  return M_PI_2 - predictable_asin(x);
}

// -----------------------------------------------------------
// Predictable sqrt, simple adaptation of an algorithm
// looping up to reaching a given precision
// Here the number of loop iterations  NB_ITER_SQRT is fixed
// (the higher, the better the precision)
// -----------------------------------------------------------
#define NB_ITER_SQRT 40
double predictable_sqrt(double val)
{
 double x = val / 2;
 for (unsigned int i=0;i<NB_ITER_SQRT;i++) {
  x = (x + val / x) / 2;
 }
 return x;
}

// -----------------------------------------------------------
// Predictable arc cos using LUT, parameter is random
// -----------------------------------------------------------
double predictable_atan(double x) {
  // Exploit the fact that atan(x) = asin(x/sqrt(x2+1))
  return predictable_asin(x / predictable_sqrt(x*x+1));
}
