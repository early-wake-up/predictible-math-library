#include <math.h>
#include "Predictable_Math.h"

// -----------------------------------------------------------
// Test procedure
// -----------------------------------------------------------
#define NB_TESTS 10000

int main() {

  // Test procedure
  double sum_difference_sin = 0.0;
  double sum_difference_cos = 0.0;
  double sum_difference_asin = 0.0;
  double sum_difference_acos = 0.0;
  double sum_difference_atan = 0.0;

  for (int i=0;i<NB_TESTS;i++) {

    // Select an angle in interval [0,2*PI] - and a value in [-1,1]
    double angle = (double)(random() % 1000000)*2*M_PI / (double)1000000.0;
    double val = (angle - M_PI) / M_PI;
    double x = (double)(random());

    // Select a fully random angle
    // double angle = (double)(random());

    // Debug
    printf("Angle %f \tsin %f \tpredictable_sin %f \tdifference %f\n",
	   angle,
	   sin(angle),predictable_sin(angle),
	   abs(sin(angle)-predictable_sin(angle)));
    sum_difference_sin += abs(sin(angle)-predictable_sin(angle));
  
    printf("Angle %f \tcos %f \tpredictable_cos %f \tdifference %f\n",
	   angle,
	   cos(angle),predictable_cos(angle),
	   abs(cos(angle)-predictable_cos(angle)));
    sum_difference_cos += abs(cos(angle)-predictable_cos(angle));

    printf("Value %f \tasin %f \tpredictable_asin %f \tdifference %f\n",
	   val,
	   asin(val),predictable_asin(val),
	   abs(asin(val)-predictable_asin(val)));
    sum_difference_asin += abs(asin(val)-predictable_asin(val));

    printf("Value %f \tacos %f \tpredictable_acos %f \tdifference %f\n",
	   val,
	   acos(val),predictable_acos(val),
	   abs(acos(val)-predictable_acos(val)));
    sum_difference_acos += abs(acos(val)-predictable_acos(val));

    printf("Value %f \tatan %f \tpredictable_atan %f \tdifference %f\n",
	   x,
	   atan(x),predictable_atan(x),
	   abs(atan(x)-predictable_atan(x)));
    sum_difference_atan += abs(atan(x)-predictable_atan(x));
 }
  printf("Overall average difference sin %f cos %f asin %f acos %f atan %f\n",
	 sum_difference_sin/ NB_TESTS,
	 sum_difference_cos/ NB_TESTS,
	 sum_difference_asin/ NB_TESTS,
	 sum_difference_acos/ NB_TESTS,
	 sum_difference_atan/ NB_TESTS);
}
