all: GeneLut TestLut

GeneLut: GeneLut.cc
	gcc -o GeneLut GeneLut.cc -lm

Predictable_Math.o: Predictable_Math.h
	gcc -c Predictable_Math.cc

TestLut: TestLut.cc lut.cc Predictable_Math.o
	gcc -o TestLut TestLut.cc Predictable_Math.o -lm

rungene:
	./GeneLut 500 lut.cc
runtest:
	./TestLut

clean:
	rm -f GeneLut TestLut Predictable_Math.o
