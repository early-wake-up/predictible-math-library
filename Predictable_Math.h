#include <stdio.h>
#include <assert.h>

// Useful macros
// -----------------------------------------------------------
#define abs(a) (((a)>0)?(a):(-1*(a)))

// Useful values from libmath.h
// -----------------------------
#define M_PI        3.14159265358979323846264338327950288   /* pi             */
#define M_PI_2      1.57079632679489661923132169163975144   /* pi/2           */
#define    NAN      __builtin_nanf("0x7fc00000")

// Exported functions, defined in Predictable_Math.cc
// --------------------------------------------------
extern double predictable_sin(double angle);
extern double predictable_cos(double angle);
extern double predictable_asin(double x);
extern double predictable_acos(double x);
extern double predictable_sqrt(double val);
extern double predictable_atan(double x);
