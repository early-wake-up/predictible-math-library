#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// Constants (from libmath)
// ------------------------
// M_PI: PI
// M_PI_2: PI/2

// Note: on macOS, libmath in the following folder
/*
/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk/usr/include/math.h
*/

// ----------------------------------------------------------------------
// Generates luts:
// - for sin in interval [0..pi/2] with exactly N elements in the LUT
//   (N-1) intervals
// - for asin in intervel [0,1] with exactly N elements in the LUT
//   (N-1) intervals
// --------------------------------------------------------------------------
void generate_lut_sin(int nbval, char *filename) {

  // Open filename and exits if impossible
  FILE *f = fopen(filename,"w");
  if (f==(FILE*) NULL) {
    fprintf(stderr,"Impossible to open LUT file %s for writing, exiting ...\n",filename);
    exit(-1);
  }

  // Generate header of LUT file
  fprintf(f,"#define N %d\n",nbval);

  // Generate the values for sine using libmath
  fprintf(f,"double lut_sin[N] = {\n");
  for (int i=0;i<nbval;i++) {
    fprintf(f,"%f",sin(M_PI_2/ (double)(nbval-1) * (double)i));
    if (i!=nbval-1) fprintf(f,", "); else fprintf(f,"};\n\n");
  }

  // Generate the values for asine using libmath
  fprintf(f,"double lut_asin[N] = {\n");
  for (int i=0;i<nbval;i++) {
    fprintf(f,"%f",asin(1.0/ (double)(nbval-1) * (double)i));
    if (i!=nbval-1) fprintf(f,", "); else fprintf(f,"};\n");
  }
  
  // Finally close the file
  fclose(f);
}

int main(int argc, char**argv) {

  if (argc!=3) {
    printf("Usage: %s <nbelems> <lutfile>\n",argv[0]);
    exit(-1);
  }
  int nbval = atoi(argv[1]);
  generate_lut_sin(nbval,argv[2]);
}
