Directory contents
------------------

README.txt		this file
Makefile		home made very basic makefile
GeneLut.cc		Generation of LUTs for sine and arc sine (parameters are number of points in LUT + filename)
Predictable_Math.cc/h 	Code for sin/asin/cos/acos/atan using LUTs, no include/link to libm required
TestLut.cc		Test of Predictable_Math and comparison with functions in libm to evaluate their precision
lut.cc			Automatically generated file, included in Predictable_Math.cc, containing the LUTs

More comments
-------------

- interface same as libc, function name = predictable_xxx (xxx in sin/cos/asin/acos/atan)
- LUT size of N (parameter to GeneLut): N points in array, N-1 intervals
- linear regression on every interval
- LUT for sin on interval [0,pi/2]
- properties of sin used to calculate sin whatever the angle given as parameter:
  	     sin(pi-x) = sin(x)
	     sin(-x)=-sin(x)
- link cos/sin used to avoid using a LUT for cos: cox(x) = sin(x-pi/2)
- acos calculated using asin
- atan computed using formula: atan(x) = asin(x/sqrt(x2+1))
  no LUT used for atan (because input in [-infty,+infty]
  predictable implementation of sqrt (no need for libm): fixed number of iterations (see function code) to
  find the sqrt, the higher the better the precision. Set experimentally in the code to reach a correct precision

Values to set to find precision/space tradeoff
----------------------------------------------

- number of points in LUT (parameter of GeneLut)
- (constant) number of iterations in predictable_sqrt (defined using a #define in the source file)

Copyright (c) 2019 PUAUT Isabelle
